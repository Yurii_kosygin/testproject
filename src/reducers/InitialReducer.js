import * as actionTypes from '../constants/InitialConstants';

const initialState = {
  token: ''
};

export default function InitialReducer(state = initialState, action) {
  const {
    token,
    type
  } = action;

  switch (type) {
    case actionTypes.GET_INPUT_INFO:
      return {
        ...state,
        token
      };

    default:
      return state;
  }
}
