import * as actionTypes from '../constants/CommentsConstants';

const initialState = {
  comments: []
};

export default function CommentsReducer(state = initialState, action) {
  const {
    comments,
    type
  } = action;

  switch (type) {
    case actionTypes.GET_COMMENTS:
      return {
        ...state,
        comments
      };

    default:
      return state;
  }
}