/* eslint-disable no-multi-spaces */

import { combineReducers } from 'redux';

import InitialReducer        from './InitialReducer';
import CommentsReducer       from './CommentsReducer';

const rootReducer = combineReducers({
  InitialReducer,
  CommentsReducer
});

export default rootReducer;
