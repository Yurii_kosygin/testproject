import './Authorization.styl';
import React, {
  Component,
  // PropTypes as Type,
} from 'react';

import { apiCall } from 'utils';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { Link } from 'react-router';

const buttonStyle = {
  width: 150,
  color: '#fff'
};
const inputStyle = {
  width: '100%'
};

export default class Authorization extends Component {

  static propTypes = {};

  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: ''
    };
  }

  _login = (event) => {
    this.setState({
      login: event.target.value,
    });
  };

  _password = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  _clickRegistration() {
    const { login, password } = this.state;

    apiCall({
      method: 'POST',
      path: '/users/register',
      data: JSON.stringify({
        email: login,
        password,
      })
    })
      .then(() => {
        this.setState({
          login: '',
          password: '',
          ...(stateProperties || null)
        });
        console.log("yep")
      })
      .catch((response) => {
        if (response.status >= 400) {
          throw new Error('Bad response from server');
        }
        return response;
      });
  };

  _logIn() {
    const { login, password } = this.state;
    localStorage.clear();
    apiCall({
      method: 'POST',
      path: '/users/login',
      data: JSON.stringify({
        email: login,
        password,
      })
    })
      .then((response) => {
        localStorage.setItem('token', response.data.accessToken);
        this.setState({
          login: '',
          password: '',
          ...(stateProperties || null)
        });
      })
      .catch((response) => {
        if (response.status >= 400) {
          throw new Error('Bad response from server');
        }
        return response;
      });
  }

  render() {
    return (
      <div className="authorization-root">
        <TextField
          hintText="E-mail"
          style={inputStyle}
          value={this.state.login}
          onChange={this._login}
        />
        <TextField
          hintText="Password"
          style={inputStyle}
          value={this.state.password}
          onChange={this._password}
        />
        <div className="button-block">
          <Link
            to="comments"
          >
            <RaisedButton
              label="Log in"
              buttonStyle={buttonStyle}
              backgroundColor="#54FF9F"
              onClick={::this._logIn}
            />
          </Link>
          <RaisedButton
            label="Registration"
            buttonStyle={buttonStyle}
            backgroundColor="#1E90FF"
            onClick = {::this._clickRegistration}
          />
        </div>
      </div>
    );
  }
}
