import * as actionTypes from '../constants/InitialConstants';


export function setInput(email, password) {
  return {
    type: actionTypes.GET_INPUT_INFO,
    email,
    password
  };
}
