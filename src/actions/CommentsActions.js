import * as actionTypes from '../constants/CommentsConstants';
import { apiCall } from 'utils';

export function getComments() {
  return function (dispatch) {
    apiCall({
      method: 'GET',
      path: '/comments',
    })
      .then((response) => {
        dispatch({
          type: actionTypes.GET_COMMENTS,
          comments: response.data,
        });
      })
      .catch((response) => {
        if (response.status >= 400) {
          throw new Error('Bad response from server');
        }
        return response;
      });
  };
}
