import './InitialContainer.styl';

import React, {
  Component,
  PropTypes as Type,
} from 'react';

import {
  Authorization
} from  'components'

import { bindActionCreators }       from 'redux';
import { connect }                  from 'react-redux';

import {
  InitialActions,
} from 'actions';

@connect(state => ({
  inputInfo: state.InitialReducer
}), dispatch => ({
  actions: bindActionCreators(InitialActions, dispatch)
}))
export default class InitialContainer extends Component {

  static propTypes = {
    actions: Type.object,
    inputInfo: Type.object
  };

  componentDidMount() {}

  render() {
    return (
      <div className="initial-container-root">
        <div className="authorization-wrapper">
          <Authorization />
        </div>
      </div>
    );
  }
}
