import './CommentsContainer.styl';

import { apiCall } from 'utils';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { Link } from 'react-router';

import Slider from 'react-slick';

import React, {
  Component,
  PropTypes as Type,
} from 'react';

const moment = require('moment');

import { bindActionCreators }       from 'redux';
import { connect }                  from 'react-redux';

import {
  CommentsActions,
} from 'actions';


const buttonStyle = {
  width: 150,
  color: '#fff'
};
const inputStyle = {
  width: '100%'
};

@connect(state => ({
  comments: state.CommentsReducer
}), dispatch => ({
  actions: bindActionCreators(CommentsActions, dispatch)
}))

export default class CommentsContainer extends Component {
  /**
   * Validates passed properties
   */
  static propTypes = {};

  constructor(props) {
    super(props);

    this.state = {
      text: ''
    };
  }

  componentDidMount() {
    this.props.actions.getComments();
    console.log(localStorage.getItem("token"));
  }

  _comment = (event) => {
    this.setState({
      text: event.target.value,
    });
  };

  static onEnter(nextState) {}

  _sendComment() {
    const { text } = this.state;

    apiCall({
      method: 'POST',
      path: '/comments',
      data: JSON.stringify({
        text
      }),
      headers: {
        "authorization": "JWT " + localStorage.getItem("token")
      }
    })
      .then(() => {
        this.props.actions.getComments();
      })
      .catch((response) => {
        if (response.status >= 400) {
          throw new Error('Bad response from server');
        }
        return response;
      });
  };

  _Exit() {
    localStorage.clear();
  }

  render() {
    const { comments } = this.props.comments;
    const settings = {
      dots: true,
      arrows: false,
      slidesToShow: 2,
      customPaging: function(i) {
        return <a></a>
      },
      dotsClass: 'pagination dots',
    };
    return (
      <div className="comments-container-root">
        {
          comments.length &&
          <Slider {...settings}>
            {comments.map((item, index) => (
              <div className="comments-block" key={index}>
                <div className="comments-response">Name: {item.email}</div>
                <div className="comments-response">Date: {moment(item.createdAt).format('lll')}</div>
                <div className="text">Comments</div>
                <div className="comments-response">{item.text}</div>
              </div>
            ))}
          </Slider>
        }
        <div className="add-comment-block">
          <TextField
            hintText="Comment..."
            style={inputStyle}
            value={this.state.text}
            onChange={this._comment}
          />
          <RaisedButton
            label="Send comment"
            buttonStyle={buttonStyle}
            backgroundColor="#1E90FF"
            onClick={::this._sendComment}
          />
        </div>
        <Link
          to="/"
        >
          <RaisedButton
            label="Exit"
            buttonStyle={buttonStyle}
            backgroundColor="#54FF9F"
            onClick={::this._Exit}
          />
        </Link>
      </div>
    );
  }
}
