import qs               from 'query-string';
import request          from 'axios';

export default (params) => {
  const method = params.method;
  const apiHost = 'http://109.120.189.5:8081';

  const query = params.query ? `?${ qs.stringify(params.query) }` : '';

  const url = `${ params.host || apiHost }${ params.path }${ query }`;
  const responseType = 'json';
  const headers = Object.assign({}, { "content-type": "application/json" }, params.headers || {});

  const requestParams = { method, url, responseType, headers };

  if (params.data) requestParams.data = params.data;

  return request(requestParams);
};
